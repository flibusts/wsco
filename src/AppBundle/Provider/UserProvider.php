<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 13:11
 */

namespace AppBundle\Provider;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserProvider
 */
class UserProvider
{
    /**
     * repository
     *
     * @var UserRepository
     */
    private $repository;

    /**
     * IssueProvider constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->repository = $manager->getRepository('AppBundle:User');
    }

    /**
     * getAll
     *
     * @return User[]
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * getAllUnlessCurrent
     *
     * @param string $username
     *
     * @return User[]
     */
    public function getAllUnlessCurrent($username)
    {
        return $this->repository->findAllUnlessCurrent($username);
    }

    /**
     * getOne
     *
     * @param int $login
     *
     * @return User
     */
    public function getOne($login)
    {
        return $this->repository->loadUserByUsername($login);
    }
}
