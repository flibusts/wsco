<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 11:53
 */

namespace AppBundle\Provider;

use AppBundle\Entity\Label;
use AppBundle\Repository\LabelRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class LabelProvider
 */
class LabelProvider
{
    /**
     * repository
     *
     * @var LabelRepository
     */
    private $repository;

    /**
     * LabelProvider constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository('AppBundle:Label');
    }

    /**
     * getAll
     *
     * @return Label[]
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * getByNames
     *
     * @param array $names
     *
     * @return Label[]|array
     */
    public function getByNames($names)
    {
        return $this->repository->findBy([
            'label' => $names,
        ]);
    }
}
