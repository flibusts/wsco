<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 12:13
 */

namespace AppBundle\Provider;

use AppBundle\Entity\Issue;
use AppBundle\Repository\IssueRepository;
use AppBundle\Service\Tools;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class IssueProvider
 */
class IssueProvider
{
    /**
     * repository
     *
     * @var IssueRepository
     */
    private $repository;

    /**
     * IssueProvider constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->repository = $manager->getRepository('AppBundle:Issue');
    }

    /**
     * getAll
     *
     * @return Issue[]
     */
    public function getAll()
    {
        return $this->getOpenAndClosed($this->repository->findAll());
    }

    /**
     * getOne
     *
     * @param int $id
     *
     * @return Issue
     */
    public function getOne($id)
    {
        return $this->repository->find($id);
    }

    /**
     * getAllFiltered
     *
     * @param array $filters
     *
     * @return Issue[]|array
     */
    public function getAllFiltered($filters)
    {
        $issues = $this->getAll();
        foreach ($filters as $filter) {
            $issues = array_intersect($this->getByFilter($filters), $issues['all']);
        }

        return $this->getOpenAndClosed($issues);
    }

    /**
     * getByFilter
     *
     * @param array $filter
     *
     * @return Issue[]|array
     */
    private function getByFilter($filter)
    {
        $filtered = [];

        if (array_key_exists('label', $filter)) {
            $filtered = $this->repository->findByLabel($filter['label']);
        } elseif (array_key_exists('title', $filter)) {
            $filtered = $this->repository->findByTitle($filter['title']);
        } elseif (array_key_exists('user', $filter)) {
            $filtered = $this->repository->findByUser($filter['user']);
        }

        return $filtered;
    }

    /**
     * getOpenAndClosed
     *
     * @param Issue[] $issues
     *
     * @return array
     */
    private function getOpenAndClosed($issues)
    {
        $opened = [];
        $closed = [];
        foreach ($issues as $issue) {
            if ($issue->isActive()) {
                $opened[] = $issue;
            } else {
                $closed[] = $issue;
            }
        }

        return [
            'all' => $issues,
            'opened' => $opened,
            'closed' => $closed,
        ];
    }
}
