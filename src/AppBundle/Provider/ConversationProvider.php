<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 21/05/2018
 */

namespace AppBundle\Provider;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\User;
use AppBundle\Repository\ConversationRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class ConversationProvider
 */
class ConversationProvider
{
    /**
     * repository
     *
     * @var ConversationRepository
     */
    private $repository;

    /**
     * ConversationProvider constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->repository = $manager->getRepository('AppBundle:Conversation');
    }

    /**
     * getAllByUser
     *
     * @param User $user
     *
     * @return Conversation[]
     */
    public function getAllByUser($user)
    {
        return $this->repository->findAllByUser($user->getId());
    }

    /**
     * getOne
     *
     * @param $id
     *
     * @return Conversation
     */
    public function getOne($id)
    {
        return $this->repository->find($id);
    }
}
