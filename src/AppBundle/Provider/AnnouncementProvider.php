<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24/03/2018
 */

namespace AppBundle\Provider;

use AppBundle\Entity\Announcement;
use AppBundle\Repository\AnnouncementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AnnouncementProvider
 */
class AnnouncementProvider
{
    /**
     * repository
     *
     * @var AnnouncementRepository
     */
    private $repository;

    /**
     * request
     *
     * @var Request
     */
    private $request;

    /**
     * pagination
     *
     * @var
     */
    private $pagination;

    /**
     * AnnouncementProvider constructor.
     *
     * @param EntityManagerInterface $emanager
     * @param RequestStack           $stack
     * @param int                    $pagination
     */
    public function __construct(EntityManagerInterface $emanager, RequestStack $stack, $pagination)
    {
        $this->repository = $emanager->getRepository('AppBundle:Announcement');
        $this->request = $stack->getCurrentRequest();
        $this->pagination = $pagination;
    }

    /**
     * @param int $start
     * @param int $stop
     *
     * @return Announcement[]
     */
    public function getAllPaginate($start = null, $stop = null)
    {
        if (empty($stop)) {
            $stop = $this->pagination;
        }
        if (empty($start)) {
            $page = $this->request->get('page');
            $page = $page > 1 ? $page : 1;
            $start = ($page - 1) * $stop;
        }

        return $this->repository->findAllPaginate($start, $stop);
    }

    /**
     * @param int $id
     *
     * @return Announcement
     */
    public function getOneById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return Announcement[]
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }
}
