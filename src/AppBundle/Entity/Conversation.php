<?php

namespace AppBundle\Entity;

use AppBundle\Traits\MessageTrait;

/**
 * Conversation
 */
class Conversation
{
    use MessageTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $firstUser;

    /**
     * @var User
     */
    private $secondUser;

    /**
     * lastMessage
     *
     * @var Message
     */
    private $lastMessage;

    /**
     * getId
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setId
     *
     * @param int $id
     *
     * @return Conversation
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * getFirstUser
     *
     * @return User
     */
    public function getFirstUser()
    {
        return $this->firstUser;
    }

    /**
     * setFirstUser
     *
     * @param User $firstUser
     *
     * @return Conversation
     */
    public function setFirstUser($firstUser)
    {
        $this->firstUser = $firstUser;

        return $this;
    }

    /**
     * getSecondUser
     *
     * @return User
     */
    public function getSecondUser()
    {
        return $this->secondUser;
    }

    /**
     * setSecondUser
     *
     * @param User $secondUser
     *
     * @return Conversation
     */
    public function setSecondUser($secondUser)
    {
        $this->secondUser = $secondUser;

        return $this;
    }

    /**
     * getLastMessage
     *
     * @return Message
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * setLastMessage
     *
     * @param Message $lastMessage
     *
     * @return Conversation
     */
    public function setLastMessage($lastMessage)
    {
        $this->lastMessage = $lastMessage;

        return $this;
    }
}

