<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06/04/2018
 */

namespace AppBundle\Entity;

use AppBundle\Traits\ActivableTrait;
use AppBundle\Traits\TaggableTrait;
use AppBundle\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Issue
 */
class Issue
{
    use TimestampableTrait, ActivableTrait, TaggableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var User
     */
    private $creator;

    /**
     * @var User
     */
    private $closedBy;

    /**
     * @var \DateTime
     */
    private $closedAt;

    /**
     * @var Comment[]
     */
    private $comments;

    /**
     * Issue constructor.
     */
    public function __construct()
    {
        $this->labels = new ArrayCollection();
    }

    /**
     * getId
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setId
     *
     * @param int $id
     *
     * @return Issue
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * getTitle
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * setTitle
     *
     * @param string $title
     *
     * @return Issue
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * getContent
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * setContent
     *
     * @param string $content
     *
     * @return Issue
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * getCreator
     *
     * @return User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * setCreator
     *
     * @param User $creator
     *
     * @return Issue
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * getClosedBy
     *
     * @return User
     */
    public function getClosedBy()
    {
        return $this->closedBy;
    }

    /**
     * setClosedBy
     *
     * @param User $closedBy
     *
     * @return Issue
     */
    public function setClosedBy($closedBy)
    {
        $this->closedBy = $closedBy;

        return $this;
    }

    /**
     * getClosedAt
     *
     * @return \DateTime
     */
    public function getClosedAt()
    {
        return $this->closedAt;
    }

    /**
     * setClosedAt
     *
     * @param \DateTime $closedAt
     *
     * @return Issue
     */
    public function setClosedAt($closedAt)
    {
        $this->closedAt = $closedAt;

        return $this;
    }

    /**
     * getComments
     *
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * setComments
     *
     * @param Comment[] $comments
     *
     * @return Issue
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * addComment
     *
     * @param Comment $comment
     *
     * @return Issue
     */
    public function addComment($comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * __toString
     *
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }
}
