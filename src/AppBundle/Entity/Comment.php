<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 11:04
 */

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableTrait;

/**
 * Class Comment
 */
class Comment
{
    use TimestampableTrait;

    /**
     * id
     *
     * @var int
     */
    private $id;

    /**
     * user
     *
     * @var User
     */
    private $user;

    /**
     * title
     *
     * @var string
     */
    private $title;

    /**
     * content
     *
     * @var string
     */
    private $content;

    /**
     * issue
     *
     * @var Issue
     */
    private $issue;

    /**
     * getId
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setId
     *
     * @param int $id
     *
     * @return Comment
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * getUser
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * setUser
     *
     * @param User $user
     *
     * @return Comment
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * getTitle
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * setTitle
     *
     * @param string $title
     *
     * @return Comment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * getContent
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * setContent
     *
     * @param string $content
     *
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * getIssue
     *
     * @return Issue
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * setIssue
     *
     * @param Issue $issue
     *
     * @return Comment
     */
    public function setIssue($issue)
    {
        $this->issue = $issue;

        return $this;
    }
}
