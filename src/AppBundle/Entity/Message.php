<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 21/05/2018
 */


namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableTrait;

/**
 * Message
 */
class Message
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var User
     */
    private $sender;

    /**
     * @var User
     */
    private $receiver;

    /**
     * conversation
     *
     * @var Conversation
     */
    private $conversation;

    /**
     * getId
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setId
     *
     * @param int $id
     *
     * @return Message
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * getContent
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * setContent
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * getSender
     *
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * setSender
     *
     * @param User $sender
     *
     * @return Message
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * getReceiver
     *
     * @return User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * setReceiver
     *
     * @param User $receiver
     *
     * @return Message
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * getConversation
     *
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * setConversation
     *
     * @param Conversation $conversation
     *
     * @return Message
     */
    public function setConversation($conversation)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * __toString
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getContent();
    }
}
