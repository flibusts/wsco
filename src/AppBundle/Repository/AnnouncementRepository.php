<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 26/03/2018
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Announcement;

/**
 * AnnouncementRepository
 */
class AnnouncementRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * find Announcements from start top stop
     *
     * @param int $start
     * @param int $stop
     *
     * @return Announcement[]
     */
    public function findAllPaginate($start, $stop)
    {
        return $this->createQueryBuilder('a')
            ->where('a.active = :true')
            ->setParameter('true', true)
            ->setFirstResult($start)
            ->setMaxResults($stop)
            ->orderBy('a.createdAt', 'desc')
            ->getQuery()
            ->getResult();
    }

    /**
     * findAll ordered
     *
     * @return Announcement[]
     */
    public function findAll()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', 'desc')
            ->getQuery()
            ->getResult();
    }
}
