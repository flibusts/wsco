<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 26/03/2018
 */

namespace AppBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * UserRepository
 */
class UserRepository extends \Doctrine\ORM\EntityRepository implements UserLoaderInterface
{
    /**
     * loadUserByUsername
     *
     * @param string $username
     *
     * @return UserInterface
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * findAllUnlessCurrent
     *
     * @param string $username
     *
     * @return UserInterface[]
     */
    public function findAllUnlessCurrent($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username <> :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult();
    }
}
