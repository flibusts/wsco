<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 29/03/2018
 * Time: 12:29
 */

namespace AppBundle\Service;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\User;

/**
 * Class Tools
 */
class Tools
{
    /**
     * slugify
     *
     * @param string $string
     *
     * @return string
     */
    public static function slugify($string)
    {
        $string = str_replace([' ', "\'"], '-', $string);
        $string = strtolower($string);

        return $string;
    }

    /**
     * getOtherUser
     *
     * @param Conversation $conversation
     * @param User         $user
     *
     * @return User
     */
    public static function getOtherUser(Conversation $conversation, User $user)
    {
        if ($conversation->getFirstUser()->getEmail() === $user->getEmail()) {
            $otherUser = $conversation->getSecondUser();
        } else {
            $otherUser = $conversation->getFirstUser();
        }

        return $otherUser;
    }
}
