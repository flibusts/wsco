<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Comment controller.
 *
 */
class CommentController extends Controller
{
    /**
     * Displays a form to edit an existing comment entity.
     *
     * @param Request $request
     * @param Comment $comment
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Comment $comment)
    {
        if (!$this->getUser()->getUsername() === $comment->getUser()->getUsername()) {
            return $this->redirectToRoute('issues_show', ['id' => $comment->getIssue()->getId()]);
        }
        $editForm = $this->createForm(CommentType::class, $comment);
        $deleteForm = $this->createDeleteForm($comment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('issues_show', ['id' => $comment->getIssue()->getId()]);
        }

        return $this->render('comment/edit.html.twig', array(
            'comment' => $comment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * deleteAction
     * Deletes the comment and redirect to original Issue
     *
     * @param Comment $comment
     *
     * @return RedirectResponse
     */
    public function deleteAction(Comment $comment)
    {
        if ($this->getUser()->getUsername() === $comment->getUser()->getUsername()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
        }

        return $this->redirectToRoute('issues_show', ['id' => $comment->getIssue()->getId()]);
    }

    /**
     * createDeleteForm
     * Creates a form to delete a comment entity.
     *
     * @param Comment $comment
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Comment $comment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comment_delete', ['id' => $comment->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
