<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 26/03/2018
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ImportType;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 */
class UserController extends Controller
{
    /**
     * adminAction
     * This route is for the admin to create new Users
     *
     * @param Request $request
     *
     * @return Response
     */
    public function adminAction(Request $request)
    {
        $user = new User();
        $formUser = $this->createForm(UserType::class, $user);
        $formImport = $this->createForm(ImportType::class);
        $formUser->handleRequest($request);
        $formImport->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $this->get('member_manager')->createOrUpdate($formUser->getData(), true);

            return $this->redirectToRoute('members_index');
        }

        if ($formImport->isSubmitted() && $formImport->isValid()) {
            $file = $formImport->getData()['file'];
            $this->get('member_manager')->insertFromFile($file);

            return $this->redirectToRoute('members_index');
        }

        return $this->render('members/admin.html.twig', [
            'formUser' => $formUser->createView(),
            'formImport' => $formImport->createView(),
        ]);
    }

    /**
     * accountAction
     * Displays the current User's profile with forms to change his informations
     *
     * @param Request $request
     *
     * @return Response
     */
    public function accountAction(Request $request)
    {
        $user = $this->getUser();
        $formUser = $this->createForm(UserType::class, $user);
        $formPasswd = $this->createForm(ChangePasswordType::class, $user);

        $formUser->handleRequest($request);
        $formPasswd->handleRequest($request);
        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        if ($formPasswd->isSubmitted()) {
            if ($formPasswd->isValid()) {
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updatePassword($user);
                $this->getDoctrine()->getManager()->flush();
            } else {
                foreach ($formPasswd->all() as $child) {
                    if (!$child->isValid()) {
                        $this->get('session')->getFlashBag()->add('warning', $child->getTransformationFailure()->getMessage());
                    }
                }
            }
        }

        return $this->render('members/profile.html.twig', [
            'form_account' => $formUser->createView(),
            'form_passwd' => $formPasswd->createView(),
        ]);
    }
}
