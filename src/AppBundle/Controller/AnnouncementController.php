<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24/03/2018
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Announcement;
use AppBundle\Form\AnnouncementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Announcement controller.
 *
 */
class AnnouncementController extends Controller
{
    /**
     * Lists all actives announcement entities on home page.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $start = $request->get('start');
        $stop = $request->get('limit');
        $announcements = $this->get('announcement_provider')->getAllPaginate($start, $stop);

        return $this->render('announcement/index.html.twig', array(
            'announcements' => $announcements,
        ));
    }

    /**
     * Edit or create new Announcement
     *
     * @param Request $request
     * @param int     $id the id of the announcement
     *
     * @return RedirectResponse|Response
     */
    public function postAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('announcement_index');
        }

        $announcement = new Announcement();
        $create = true;
        if (!empty($id)) {
            $annTmp = $this->get('announcement_provider')->getOneById($id);
            if ($annTmp instanceof Announcement) {
                $announcement = $annTmp;
                $create = false;
            }
        }
        $form = $this->createForm(AnnouncementType::class, $announcement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('announcement_manager')->createOrUpdate($form->getData(), $create);

            return $this->redirectToRoute('announcement_index');
        }

        return $this->render('announcement/new.html.twig', array(
            'title' => $create ? 'Nouvelle annonce' : 'Editer '.$announcement->getTitle(),
            'announcement' => $announcement,
            'form' => $form->createView(),
        ));
    }

    /**
     * This action lists all announcements even the deactiaveted ones
     *
     * @return RedirectResponse|Response
     */
    public function listAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('announcement_index');
        }

        $announcements = $this->get('announcement_provider')->getAll();

        return $this->render('announcement/list.html.twig', array(
            'announcements' => $announcements,
        ));
    }

    /**
     * Change the state of an announcement to active/non active
     *
     * @param int $id id of the announcement
     *
     * @return RedirectResponse
     */
    public function activateAction($id)
    {
        $announcement = $this->get('announcement_provider')->getOneById($id);
        $announcement->setActive(!$announcement->isActive());
        $this->get('announcement_manager')->createOrUpdate($announcement);

        return $this->redirectToRoute('announcement_list');
    }
}
