<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Issue;
use AppBundle\Form\CommentType;
use AppBundle\Form\IssueType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Issue controller.
 *
 */
class IssueController extends Controller
{
    /**
     * indexAction
     * List of all issues with or without filter
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        if (!empty($request->query->all())) {
            $issues = $this->get('issue_provider')->getAllFiltered($request->query->all());
        } else {
            $issues = $this->get('issue_provider')->getAll();
        }

        return $this->render('issue/index.html.twig', array(
            'issues' => $issues['all'],
            'opened' => $issues['opened'],
            'closed' => $issues['closed'],
        ));
    }

    /**
     * Creates a new issue entity.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $issue = new Issue();
        $user = $this->get('user_provider')->getOne($this->getUser()->getUsername());
        $form = $this->createForm(IssueType::class, $issue, ['user' => $user]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('issue_manager')->createOrUpdate($form->getData(), true);

            return $this->redirectToRoute('issues_show', array('id' => $issue->getId()));
        }

        return $this->render('issue/new.html.twig', array(
            'issue' => $issue,
            'form' => $form->createView(),
        ));
    }

    /**
     * showAction
     * Displays an Issue with all its Comments
     * Creates a new Comment for the Issue
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $issue = $this->get('issue_provider')->getOne($id);
        $stateForm = $this->createChangeStateForm($issue);
        $formComment = $this->createCommentForm($issue);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $this->get('comment_manager')->createOrUpdate($formComment->getData(), true);

            return $this->redirectToRoute('issues_show', ['id' => $issue->getId()]);
        }

        return $this->render('issue/show.html.twig', array(
            'issue' => $issue,
            'state_form' => $stateForm->createView(),
            'form_comment' => $formComment->createView(),
        ));
    }

    /**
     * editAction
     * Displays a form to edit an existing issue entity.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $issue = $this->get('issue_provider')->getOne($id);
        $user = $this->get('user_provider')->getOne($this->getUser()->getUsername());

        $editForm = $this->createForm(IssueType::class, $issue, ['user' => $user]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->get('issue_manager')->createOrUpdate($editForm->getData());

            return $this->redirectToRoute('issues_show', array('id' => $issue->getId()));
        }

        return $this->render('issue/edit.html.twig', array(
            'issue' => $issue,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * changeStateAction
     * Method to change state to active/inactive
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function changeStateAction($id)
    {
        $issue = $this->get('issue_provider')->getOne($id);
        $issue->setActive(!$issue->isActive());
        $this->get('issue_manager')->createOrUpdate($issue);

        return $this->redirectToRoute('issues_show', array('id' => $issue->getId()));
    }

    /**
     * createDeleteForm
     * Creates a form to patch an issue entity.
     *
     * @param Issue $issue
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createChangeStateForm(Issue $issue)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('issues_state', array('id' => $issue->getId())))
            ->setMethod('PATCH')
            ->getForm();
    }

    /**
     * createCommentForm
     * Creates a form to create a comment entity
     *
     * @param Issue $issue
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCommentForm(Issue $issue)
    {
        $comment = new Comment();
        $user = $this->get('user_provider')->getOne($this->getUser()->getUsername());
        $comment->setUser($user)
            ->setIssue($issue);

        return $this->createForm(CommentType::class, $comment, [
            'method' => 'POST',
        ]);
    }
}
