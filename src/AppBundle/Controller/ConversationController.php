<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 21/05/2018
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\Message;
use AppBundle\Form\ConversationType;
use AppBundle\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ConversationController
 */
class ConversationController extends Controller
{
    /**
     * messageAction
     * Displays all the conversation for the connected user
     *
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->getUser();
        $conversations = $this->get('conversation_provider')->getAllByUser($user);

        return $this->render('conversations/index.html.twig', [
            'conversations' => $conversations,
        ]);
    }

    /**
     * newAction
     * Creates a new Conversation with the first Message
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $convForm = $this->createFormConversation();
        $convForm->handleRequest($request);
        if ($convForm->isSubmitted() && $convForm->isValid()) {
            $conversation = $convForm->getData();
            $this->get('conversation_manager')->createOrUpdate($conversation, true, true, false);

            return $this->redirectToRoute('conversation_show', ['id' => $conversation->getId()]);
        }

        return $this->render('conversations/new.html.twig', [
            'form' => $convForm->createView(),
        ]);
    }

    /**
     * showAction
     * Displays a given Conversation with all its Messages
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Response
     */
    public function showAction(Request $request, $id)
    {
        $conversation = $this->get('conversation_provider')->getOne($id);
        $user = $this->getUser();
        if (!in_array($user->getEmail(), [$conversation->getFirstUser()->getEmail(), $conversation->getSecondUser()->getEmail()])) {
            return $this->forward('AppBundle:Conversation:index');
        }
        $formMessage = $this->createFormMessage($conversation);
        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $this->get('conversation_manager')->createOrUpdate($formMessage->getData());

            return $this->redirectToRoute('conversation_show', ['id' => $conversation->getId()]);
        }

        return $this->render('conversations/show.html.twig', [
            'conversation' => $conversation,
            'form_message' => $formMessage->createView(),
        ]);
    }

    /**
     * createFormMessage
     * Creates the form for a new Message in a conversation
     *
     * @param Conversation $conversation
     *
     * @return FormInterface
     */
    private function createFormMessage(Conversation $conversation)
    {
        $message = new Message();
        $user = $this->getUser();

        return $this->createForm(MessageType::class, $message, [
            'method' => 'POST',
            'conversation' => $conversation,
            'user' => $user,
        ]);
    }

    /**
     * createFormConversation
     * Creates the form for a new Conversation
     *
     * @return FormInterface
     */
    private function createFormConversation()
    {
        return $this->createForm(
            ConversationType::class,
            new Conversation(),
            [
                'user' => $this->getUser(),
                'message' => new Message(),
            ]
        );
    }
}
