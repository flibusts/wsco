<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06/05/2018
 */

namespace AppBundle\Listener;

use AppBundle\Entity\Label;

/**
 * Class LabelListener
 */
class LabelListener
{
    /**
     * prePersist
     *
     * @param Label $label
     */
    public function prePersist(Label $label)
    {
        if (empty($label->getColor())) {
            $label->setColor(str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT));
        }
    }
}
