<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 12:34
 */

namespace AppBundle\Listener;

use AppBundle\Entity\Issue;

/**
 * Class IssueListener
 */
class IssueListener
{
    /**
     * prePersist
     *
     * @param Issue $issue
     */
    public function prePersist(Issue $issue)
    {
        $issue->setActive(true);
        if (empty($issue->getCreatedAt())) {
            $issue->setCreatedAt(new \DateTime());
        }
    }

    /**
     * preUpdate
     *
     * @param Issue $issue
     */
    public function preUpdate(Issue $issue)
    {
        $issue->setUpdatedAt(new \DateTime());
    }
}
