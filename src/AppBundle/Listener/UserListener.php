<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 29/03/2018
 * Time: 12:28
 */

namespace AppBundle\Listener;

use AppBundle\Entity\User;

/**
 * Class UserListener
 */
class UserListener
{
    /**
     * prePersist
     *
     * @param User $user
     */
    public function prePersist(User $user)
    {
        if (!$user->getBirthDay() instanceof \DateTime) {
            $birthday = \DateTime::createFromFormat('d/m/Y', $user->getBirthDay());
            $birthday->format('Y-m-d');
            $user->setBirthDay($birthday);
        }
        $user->setEnabled(true);
    }
}
