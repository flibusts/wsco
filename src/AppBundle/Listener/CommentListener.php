<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 07/04/2018
 */

namespace AppBundle\Listener;

use AppBundle\Entity\Comment;

/**
 * Class CommentListener
 */
class CommentListener
{
    /**
     * prePersist
     *
     * @param Comment $comment
     */
    public function prePersist(Comment $comment)
    {
        if (empty($comment->getCreatedAt())) {
            $comment->setCreatedAt(new \DateTime());
        }
    }

    /**
     * preUpdate
     *
     * @param Comment $comment
     */
    public function preUpdate(Comment $comment)
    {
        $comment->setUpdatedAt(new \DateTime());
    }
}
