<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 04/06/2018
 */

namespace AppBundle\Listener;

use AppBundle\Entity\Message;
use AppBundle\Provider\ConversationProvider;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ConversationListener
 */
class ConversationListener
{
    /**
     * entityManager
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    /**
     * convProvider
     *
     * @var ConversationProvider
     */
    protected $convProvider;

    /**
     * ConversationListener constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ConversationProvider   $convProvider
     */
    public function __construct(EntityManagerInterface $entityManager, ConversationProvider $convProvider)
    {
        $this->entityManager = $entityManager;
        $this->convProvider = $convProvider;
    }

    /**
     * prePersist
     *
     * @param Message $message
     */
    public function prePersist(Message $message)
    {
        if (empty($message->getCreatedAt())) {
            $message->setCreatedAt(new \DateTime());
        }
        $message->getConversation()->setLastMessage($message);
    }
}
