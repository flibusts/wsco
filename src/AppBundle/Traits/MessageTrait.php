<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 04/06/2018
 */

namespace AppBundle\Traits;

use AppBundle\Entity\Message;

/**
 * Trait MessageTrait
 */
trait MessageTrait
{
    /**
     * messages
     *
     * @var Message[]
     */
    protected $messages;

    /**
     * getMessages
     *
     * @return Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * setMessages
     *
     * @param Message[] $messages
     *
     * @return MessageTrait
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }
}
