<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06/05/2018
 */

namespace AppBundle\Traits;

use AppBundle\Entity\Label;

/**
 * Trait TaggableTrait
 */
trait TaggableTrait
{
    /**
     * @var Label[]
     */
    private $labels;

    /**
     * getLabels
     *
     * @return Label[]
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * setLabels
     *
     * @param Label[] $labels
     *
     * @return TaggableTrait
     */
    public function setLabels($labels)
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * addLabel
     *
     * @param Label $label
     *
     * @return TaggableTrait
     */
    public function addLabel($label)
    {
        $this->labels[] = $label;

        return $this;
    }

    /**
     * removeTag
     *
     * @param Label $label
     */
    public function removeTag($label)
    {
        $this->labels->removeElement($label);
    }
}
