<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24/03/2018
 * Time: 12:18
 */

namespace AppBundle\Traits;

/**
 * Trait Activable
 */
trait ActivableTrait
{
    /**
     * active
     *
     * @var bool
     */
    protected $active;

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return ActivableTrait
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }
}
