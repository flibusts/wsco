<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 02/04/2018
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ImportType
 */
class ImportType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'Upload File',
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'button expanded',
                ],
            ]);
    }
}
