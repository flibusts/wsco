<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 26/03/2018
 * Time: 14:30
 */

namespace AppBundle\Form;

use AppBundle\Entity\Announcement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class AnnouncementType
 */
class AnnouncementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 10, 'max' => 50]),
                ],
            ])
            ->add('content', TextareaType::class, [
                'required' => true,
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 20]),
                ],
                'attr' => [
                    'rows' => 15,
                ],
            ])
            ->add('active', CheckboxType::class, [
                'required' => false,
                'value' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Announcement::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_announcement';
    }
}
