<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 21/05/2018
 */

namespace AppBundle\Form;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Service\Tools;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MessageType
 */
class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'rows' => 5,
                ],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'button success',
                ],
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options) {
                $this->onPreSetData($event, $options);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
            'conversation' => Conversation::class,
            'user' => User::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_message';
    }

    /**
     * onPreSetData
     *
     * @param FormEvent $event
     * @param array     $options
     */
    private function onPreSetData(FormEvent $event, $options)
    {
        $conversation = $options['conversation'];
        $sender = $options['user'];
        $receiver = Tools::getOtherUser($conversation, $sender);

        $event->getData()->setConversation($conversation);
        $event->getData()->setSender($sender);
        $event->getData()->setReceiver($receiver);
    }
}
