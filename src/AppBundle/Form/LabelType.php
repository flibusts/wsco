<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06/05/2018
 */

namespace AppBundle\Form;

use AppBundle\Form\Transformer\LabelTransformer;
use AppBundle\Provider\LabelProvider;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LabelType
 */
class LabelType extends AbstractType
{
    /**
     * labelProvider
     *
     * @var LabelProvider
     */
    protected $labelProvider;

    /**
     * LabelTransformer constructor.
     *
     * @param LabelProvider $labelProvider
     */
    public function __construct(LabelProvider $labelProvider)
    {
        $this->labelProvider = $labelProvider;
    }

    /**
     * buildForm
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new CollectionToArrayTransformer(), true)
            ->addModelTransformer(new LabelTransformer($this->labelProvider), true);
    }

    /**
     * configureOptions
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('required', false);
    }

    /**
     * getParent
     *
     * @return null|string
     */
    public function getParent()
    {
        return TextType::class;
    }
}
