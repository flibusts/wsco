<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06/04/2018
 */

namespace AppBundle\Form;

use AppBundle\Entity\Issue;
use AppBundle\Entity\User;
use AppBundle\Provider\LabelProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class IssueType
 */
class IssueType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length(['min' => 7, 'max' => 50]),
                ],
            ])
            ->add('content', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'rows' => 10,
                ],
            ])
            ->add('labels', LabelType::class);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options) {
                $this->onPreSetData($event, $options);
            }
        );
    }

    /**
     * onPreSetData
     *
     * @param FormEvent $event
     * @param array     $options
     */
    public function onPreSetData(FormEvent $event, $options)
    {
        if (empty($event->getData()->getCreator())) {
            $event->getData()->setCreator($options['user']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Issue::class,
            'user' => new User(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_issue';
    }
}
