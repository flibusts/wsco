<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 04/06/2018
 */

namespace AppBundle\Form;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Provider\UserProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ConversationType
 */
class ConversationType extends AbstractType
{
    /**
     * userProvider
     *
     * @var UserProvider
     */
    protected $userProvider;

    /**
     * ConversationType constructor.
     *
     * @param UserProvider $userProvider
     */
    public function __construct(UserProvider $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    /**
     * buildForm
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('secondUser', ChoiceType::class, [
                'label' => 'Destinataire',
                'multiple' => false,
                'choices' => $this->userProvider->getAllUnlessCurrent($options['user']->getUsername()),
                'choice_label' => function ($user, $key, $value) {
                    return ucfirst(strtolower($user->getUsername()));
                },
            ])
            ->add('lastMessage', MessageType::class, [
                'data' => $options['message'],
                'conversation' => $options['data'],
                'user' => $options['user'],
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options) {
                $this->onPreSetData($event, $options);
            }
        );

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($options) {
                $this->onPreSubmitData($event, $options);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conversation::class,
            'user' => User::class,
            'message' => Message::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_conversation';
    }

    /**
     * onPreSetData
     *
     * @param FormEvent $event
     * @param array     $options
     */
    private function onPreSetData(FormEvent $event, $options)
    {
        $event->getData()->setFirstUser($options['user']);
    }

    private function onPreSubmitData(FormEvent $event, $options)
    {
        $event->getData()->getLastMessage()->setReceiver($event->getData()->getSecondUser());
    }
}
