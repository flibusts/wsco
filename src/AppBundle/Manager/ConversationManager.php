<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 04/06/2018
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\User;

/**
 * Class ConversationManager
 */
class ConversationManager extends AbstractManager
{
    /**
     * createOrUpdate
     *
     * @param mixed $obj
     * @param bool  $create
     * @param bool  $flush
     * @param bool  $merge
     *
     * @return mixed|void
     */
    public function createOrUpdate($obj, $create = false, $flush = true, $merge = true)
    {
        if ($merge) {
            $this->entityManager->merge($obj);
        }
        parent::createOrUpdate($obj, $create, $flush);
    }
}
