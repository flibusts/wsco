<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 02/04/2018
 */

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Service\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class MemberManager
 */
class MemberManager extends AbstractManager
{
    /**
     * encoder
     *
     * @var UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * AnnouncementManager constructor.
     *
     * @param EntityManagerInterface       $entityManager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($entityManager);
        $this->encoder = $encoder;
    }

    /**
     * @param User $user
     * @param bool $create
     * @param bool $flush
     */
    public function createOrUpdate($user, $create = false, $flush = true)
    {
        if (empty($user->getPassword())) {
            $plainPass = Tools::slugify($user->getLastname().$user->getFirstname());
            $passHashed = $this->encoder->encodePassword($user, $plainPass);
            $user->setPassword($passHashed);
        }
        parent::createOrUpdate($user, $create, $flush);
    }

    /**
     * insertFromFile
     *
     * @param UploadedFile $file
     */
    public function insertFromFile($file)
    {
        $file = file_get_contents($file->getPathname());
        $serializer = new Serializer([new ArrayDenormalizer(), new GetSetMethodNormalizer()], [new CsvEncoder(';')]);
        $users = $serializer->deserialize($file, 'AppBundle\Entity\User[]', 'csv');
        foreach ($users as $user) {
            $this->createOrUpdate($user, true, false);
        }
        $this->entityManager->flush();
    }
}
