<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 12:01
 */

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractManager
 */
abstract class AbstractManager
{
    /**
     * entityManager
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * AbstractManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * createOrUpdate
     *
     * @param mixed $obj
     * @param bool  $create
     * @param bool  $flush
     *
     * @return mixed
     */
    public function createOrUpdate($obj, $create = false, $flush = true)
    {
        if ($create) {
            $this->entityManager->persist($obj);
        }
        if ($flush) {
            $this->entityManager->flush();
        }
    }
}
