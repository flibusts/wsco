<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24/03/2018
 */

namespace AppBundle\Manager;

use AppBundle\Provider\AnnouncementProvider;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AnnouncementManager
 */
class AnnouncementManager extends AbstractManager
{
    /**
     * provider
     *
     * @var AnnouncementProvider
     */
    protected $provider;

    /**
     * AnnouncementManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param AnnouncementProvider   $provider
     */
    public function __construct(EntityManagerInterface $entityManager, AnnouncementProvider $provider)
    {
        parent::__construct($entityManager);
        $this->provider = $provider;
    }
}
