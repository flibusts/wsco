<?php
/**
 * Created by PhpStorm.
 * User: j.gaertner
 * Date: 06/04/2018
 * Time: 12:10
 */

namespace AppBundle\Manager;

use AppBundle\Provider\LabelProvider;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class LabelManager
 */
class LabelManager extends AbstractManager
{
    /**
     * provider
     *
     * @var LabelProvider
     */
    protected $provider;

    /**
     * LabelManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LabelProvider          $provider
     */
    public function __construct(EntityManagerInterface $entityManager, LabelProvider $provider)
    {
        parent::__construct($entityManager);
        $this->provider = $provider;
    }
}
